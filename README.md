
# PHP Software Engineer Workshop

This is a catalog and the shop cart API module can be consumed by a mobile application. This module depends on three microservices, the first one is responsible to manage the catalog, the second service is responsible for managing the customer’s shop cart, and the third service responsible to send emails.


## Authors

- [@avahabp](https://gitlab.com/avahabp)


## Documentation

[API Documentation](https://gitlab.com/avahabp/tarmeez-workshop/-/blob/main/API%20Documentation.pdf)


## Installation

 Run Catalogue App 

```bash
  cd catalogue/
  ./vendor/bin/sail up
```

Run ShopCart App 

```bash
  cd shop-cart/ 
  ./vendor/bin/sail up
```

Run Email App 

```bash
  cd email/ 
  docker-compose up
```

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file of ShopCart and Email App


`KAFKA_QUEUE=default`

`BOOTSTRAP_SERVERS=pkc-ldvr1.asia-southeast1.gcp.confluent.cloud:9092`

`SECURITY_PROTOCOL=SASL_SSL`

`SASL_USERNAME=LEFF7L3ZARUN2Y3T`

`SASL_PASSWORD=KDugi/Fa5n1AKXQF+V1yNyar3czoisZdVvoRCbfVHKmnOypRCGIUTo5Fa/5jgiHF`

`SASL_MECHANISMS=PLAIN`

`GROUP_ID=mygroup`