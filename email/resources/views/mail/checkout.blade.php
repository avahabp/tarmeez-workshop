<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product Order</title>
</head>

<body>
    <h3>Order Number - {{$data['order_number']}}</h3>
    <br />
    Name : <b>{{$data['name']}}</b><br />
    Email : <b>{{$data['email']}}</b><br />
    Total Amount : <b>{{$data['total_amount']}}</b><br />
</body>

</html>