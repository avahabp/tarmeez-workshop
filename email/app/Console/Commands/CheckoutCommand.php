<?php

namespace App\Console\Commands;

use App\Jobs\CheckoutJob;
use Illuminate\Console\Command;

class CheckoutCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkout';


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Email Testing

        $data=[];
        $data['user'] = 'PAV';
        $data['email'] = 'avahabp@gmail.com';
        $data['order_number'] ="INV00123";
        $data['total_amount'] =350;
        CheckoutJob::dispatch($data);

    }
}
