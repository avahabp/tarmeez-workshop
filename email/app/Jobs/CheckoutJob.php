<?php

namespace App\Jobs;

use App\Mail\CheckoutMail;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CheckoutJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $checkiout_mail = new CheckoutMail($this->data);
        try {
            Log::error("CheckoutJob Mail sending");
            Mail::to($this->data['email'])->send($checkiout_mail);
        } catch (Exception $ex) {
            Log::error("CheckoutJob : " . $ex->getMessage());
        }
    }
}
