<?php

namespace App\Interfaces;

interface UserRepositoryInterface 
{
    public function userRegister($data);
    public function userLogin($data);
    public function getUser();
    public function logout($request);
}