<?php

namespace App\Interfaces;

interface ProductRepositoryInterface 
{
    public function getAllProducts();
    public function productStore($request);
    public function getProduct($id);
    public function productUpdate($request);
    public function productDelete($id);
}