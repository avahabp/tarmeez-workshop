<?php

namespace App\Http\Controllers;

use App\Interfaces\ProductRepositoryInterface;
use Exception;
use Illuminate\Http\Request;

class UserProductController extends Controller
{
    private ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $products =  $this->productRepository->getAllProducts();
            return response()->json([
                'success' => true,
                'status_code' => 200,
                'message' => '',
                'data' => $products,
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $product =  $this->productRepository->getProduct($id);
            return response()->json([
                'success' => true,
                'status_code' => 200,
                'message' => '',
                'data' => $product,
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }
}
