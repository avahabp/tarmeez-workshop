<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Interfaces\ProductRepositoryInterface;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    private ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllProducts()
    {
        try {
            $products =  $this->productRepository->getAllProducts();
            return response()->json([
                'success' => true,
                'status_code' => 200,
                'message' => '',
                'data' => $products,
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function productStore(ProductStoreRequest $request)
    {
        try {
            $product =  $this->productRepository->productStore($request);
            return response()->json([
                'success' => true,
                'status_code' => 200,
                'message' => 'Product saved successfully',
                'data' => $product,
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function productDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => 'Please enter valid data',
            ]);
        }
        try {
            $product =  $this->productRepository->getProduct($request->id);
            return response()->json([
                'success' => true,
                'status_code' => 200,
                'message' => '',
                'data' => $product,
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function productUpdate(ProductUpdateRequest $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'sku' => 'required|string|max:255|unique:products,sku,' . $request->id,
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => 'Please enter valid data',
            ]);
        }
        try {
            $product =  $this->productRepository->productUpdate($request);
            return response()->json([
                'success' => true,
                'status_code' => 200,
                'message' => 'Product updated successfully',
                'data' => $product,
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function productDelete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => 'Please enter valid data',
            ]);
        }
        try {
            $product =  $this->productRepository->productDelete($request->id);
            return response()->json([
                'success' => true,
                'status_code' => 200,
                'message' => 'Product deleted successfully',
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }
}
