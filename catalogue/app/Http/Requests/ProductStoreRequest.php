<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'sku' => 'required|string|max:255|unique:products',
            'price' => 'required|regex:/^\d*(\.\d{2})?$/',
            'image' => 'mimes:jpeg,jpg,png,svg,webp'
        ];
    }


    public function failedValidation(Validator $validator)

    {
        throw new HttpResponseException(response()->json([
            'success'   => false,
            'status_code' => 401,
            'message' => 'Validation errors',
            'data'      => $validator->errors()
        ]));
    }
    
     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
 
            'name.required' => 'Name is required!',
            'sku.required' => 'SKU is required!',
            'price.required' => 'Price is required!'
        ];
    }

}
