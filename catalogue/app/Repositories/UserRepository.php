<?php

namespace App\Repositories;

use App\Interfaces\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class UserRepository implements UserRepositoryInterface
{
    public function userRegister($data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        return $user;
    }

    public function userLogin($data)
    {
        if (!Auth::attempt($data)) {

            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => 'invalid credentials',
            ]);
        }
        $user = Auth::user();
        $token = $user->createToken('token')->plainTextToken;
        return response()->json([
            'success' => true,
            'status_code' => 200,
            'message' => 'User logged in successfully',
            'data' => 'Bearer ' . $token,
        ]);
    }

    public function getUser()
    {
        $user = Auth::user();
        return response()->json([
            'success' => true,
            'status_code' => 200,
            'message' => 'User details',
            'data' => $user
        ]);
    }

    public function logout($request)
    {
        $user = Auth::user();
        $user->tokens()->delete();
        //    $user->currentAccessToken()->delete();
        return response()->json([
            'success' => true,
            'status_code' => 200,
            'message' => 'User logged out',
        ]);
    }
}
