<?php

namespace App\Repositories;

use App\Interfaces\ProductRepositoryInterface;
use App\Models\Product;
use Illuminate\Support\Str;

class ProductRepository implements ProductRepositoryInterface
{
    public function getAllProducts()
    {
        $products = Product::get();
        $data   =   [];
        $i = 0;
        foreach ($products as $key => $product) {
            $data[$i]['id'] = $product->id;
            $data[$i]['name'] = $product->name;
            $data[$i]['description'] = $product->description;
            $data[$i]['sku'] = $product->sku;
            $data[$i]['price'] = $product->price;
            $data[$i]['image'] = $product->image_url;
            $i++;
        }
        return $data;
    }

    public function productStore($request)
    {
        $product =  Product::create([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'sku' => $request->sku,
        ]);
        if ($request->file('image')) {
            $extension = $request->image->clientExtension();
            $product->addMediaFromRequest('image')
                ->usingFileName(Str::random() . '.' . $extension)
                ->toMediaCollection('product_image');
        }

        return $product;
    }

    public function getProduct($id)
    {
        $product = Product::findOrFail($id);
        $data = [];
        $data['id'] = $product->id;
        $data['name'] = $product->name;
        $data['description'] = $product->description;
        $data['sku'] = $product->sku;
        $data['price'] = $product->price;
        $data['image'] = $product->image_url;
        return  $data;
    }
    public function productUpdate($request)
    {
        $product = Product::findOrFail($request->id);
        $product->update([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'sku' => $request->sku,
        ]);
        if ($request->file('image')) {
            $extension = $request->image->clientExtension();
            $product->addMediaFromRequest('image')
                ->usingFileName(Str::random() . '.' . $extension)
                ->toMediaCollection('product_image');
        }
        return $product;
    }

    public function productDelete($id)
    {
        $product = Product::findOrFail($id);
        return $product->delete();
    }
}
