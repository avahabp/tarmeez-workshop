<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductDetailsController;
use App\Http\Controllers\UserProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::post('get-user', [AuthController::class, 'getUser']);
    Route::post('logout', [AuthController::class, 'logout']);

    Route::post('product-store', [ProductController::class, 'productStore']);
    Route::post('get-all-products', [ProductController::class, 'getAllProducts']);
    Route::post('product-details', [ProductController::class, 'productDetails']);
    Route::post('product-update', [ProductController::class, 'productUpdate']);
    Route::post('product-delete', [ProductController::class, 'productDelete']);
});

//for ShopCart
Route::resource('product_details', UserProductController::class);
