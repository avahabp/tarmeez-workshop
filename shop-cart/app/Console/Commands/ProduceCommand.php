<?php

namespace App\Console\Commands;

use App\Jobs\CheckoutJob;
use Illuminate\Console\Command;

class ProduceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'produce';

 
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        CheckoutJob::dispatch();
    }
}
