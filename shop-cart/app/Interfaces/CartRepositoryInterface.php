<?php

namespace App\Interfaces;

interface CartRepositoryInterface 
{
    public function cartAddItem($request);
    public function cartDeleteItem($product_id);
    public function getCartItems();
    public function getItemDetails($id);
    public function checkout($request);
}