<?php

namespace App\Http\Controllers;

use App\Services\ProductService;
use Illuminate\Http\Request;


class ProductController extends Controller
{
    public ProductService $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function getProducts()
    {
        $data =  $this->productService->get('product_details', []);
        return $data;
    }

    public function getProductDetails(Request $request)
    {
        $data =  $this->productService->get('product_details/' . $request->id, []);
        return $data;
    }
}
