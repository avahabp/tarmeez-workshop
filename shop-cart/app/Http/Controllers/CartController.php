<?php

namespace App\Http\Controllers;

use App\Interfaces\CartRepositoryInterface;
use App\Models\Cart;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    private CartRepositoryInterface $cartRepository;

    public function __construct(CartRepositoryInterface $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function cartAddItem(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'quantity' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => 'Please enter valid data',
            ]);
        }
        try {
            $this->cartRepository->cartAddItem($request);
            return response()->json([
                'success' => true,
                'status_code' => 200,
                'message' => 'Data saved successfully',
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }

    public function cartDeleteItem(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => 'Please enter valid data',
            ]);
        }
        try {
            $this->cartRepository->cartDeleteItem($request->product_id);
            return response()->json([
                'success' => true,
                'status_code' => 200,
                'message' => 'Deleted successfully',
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }

    public function getCartItems(Request $request)
    {
        try {
            $data =   $this->cartRepository->getCartItems();
            return response()->json([
                'success' => true,
                'status_code' => 200,
                'data' => $data,
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }


    public function checkout(Request $request)
    {
        try {
            $data =   $this->cartRepository->checkout($request);
            return response()->json([
                'success' => true,
                'status_code' => 200,
                'messege' => 'Checkout successfully & Mail Sent',
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }
}
