<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Interfaces\UserRepositoryInterface;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class AuthController extends Controller
{
    //

    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function register(UserRegisterRequest $request)
    {
        try {
            $data = $request->only('name', 'email', 'password');
            $user = $this->userRepository->userRegister($data);
            return response()->json([
                'success' => true,
                'status_code' => 200,
                'message' => 'User created successfully',
                'data' => $user,
            ]);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }


    public function login(UserLoginRequest $request)
    {
        try {
            $data = $request->only('email', 'password');
            return $this->userRepository->userLogin($data);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }


    public function getUser()
    {
        try {
            return $this->userRepository->getUser();
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }

    public function logout(Request $request)
    {
        try {
            return $this->userRepository->logout($request);
        } catch (Exception $ex) {
            return response()->json([
                'success' => false,
                'status_code' => 401,
                'message' => $ex->getMessage(),
            ]);
        }
    }
}
