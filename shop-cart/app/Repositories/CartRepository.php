<?php

namespace App\Repositories;

use App\Interfaces\CartRepositoryInterface;
use App\Jobs\CheckoutJob;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Services\ProductService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartRepository implements CartRepositoryInterface
{

    public ProductService $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }


    public function cartAddItem($request)
    {

        $res =  $this->getItemDetails($request->product_id);
        $product = $res['data'];
        if ($product) {
            $user_id = Auth::user()->id;
            Cart::where('user_id', $user_id)->where('product_id', $request->product_id)->delete();
            $cart = Cart::create([
                'user_id' => $user_id,
                'product_id' => $request->product_id,
                'quantity' => $request->quantity,
                'price' => $product['price'],
            ]);
        }
        return $cart;
    }

    public function cartDeleteItem($product_id)
    {
        $user_id = Auth::user()->id;
        return Cart::where('user_id', $user_id)->where('product_id', $product_id)->delete();
    }

    public function getCartItems()
    {
        $user_id = Auth::user()->id;
        $items = Cart::where('user_id', $user_id)->get();

        $cart_items = [];
        $i = 0;
        $cart_total = 0;
        foreach ($items as $item) {
            $res =  $this->getItemDetails($item->product_id);
            $product = $res['data'];
            $cart_items[$i]['id'] = $item->id;
            $cart_items[$i]['name'] = $product['name'];
            $cart_items[$i]['product_id'] = $item->product_id;
            $cart_items[$i]['price'] = $item->price;
            $cart_items[$i]['quantity'] = $item->quantity;
            $cart_items[$i]['total'] = floatval($item->price * $item->quantity);
            $cart_total += floatval($item->price * $item->quantity);
            $i++;
        }
        $data['cart_items'] = $cart_items;
        $data['cart_total'] = $cart_total;
        return $data;
    }
    public function getItemDetails($id)
    {
        return  $this->productService->get('product_details/' . $id, []);
    }


    public function checkout($request)
    {
        $user_id = Auth::user()->id;
        $cart_items =  Cart::where('user_id', $user_id)->get();
        if ($cart_items->count() == 0) {
            return response()->json([
                'success' => false,
                'status_code' => 403,
                'message' => 'Your cart is empty',
            ]);
        }
        return DB::transaction(function () use ($request, $user_id, $cart_items) {
            $cart_total = 0;
            foreach ($cart_items as $cart_item) {
                $cart_total +=   floatval($cart_item->price * $cart_item->quantity);
            }
            $max_id = Order::max('id');
            $max_id = $max_id == null ? 1 : $max_id + 1;
            $order = Order::create([
                'order_number' => 'INV' . sprintf("%06d", $max_id),
                'user_id' => $user_id,
                'delivery_details' => $request->delivery_details,
                'total_amount' => $cart_total,
                'payment_method' => 0, //"Cash"
                'status_id' => 1, //"Ordered"
            ]);

            foreach ($cart_items as $cart_item) {
                OrderProduct::create([
                    'order_id' => $order->id,
                    'product_id' => $cart_item->product_id,
                    'quantity' => $cart_item->quantity,
                    'price' => $cart_item->price,
                ]);
            }
            Cart::where('user_id', $user_id)->delete();

            //Kafka - Email Sending

            $data = [];
            $data['user'] = Auth::user()->name;
            $data['email'] = Auth::user()->email;
            $data['order_number'] = $order->order_number;
            $data['total_amount'] = $order->total_amount;
            CheckoutJob::dispatch($data);
        });
    }
}
