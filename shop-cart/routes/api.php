<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::post('get-user', [AuthController::class, 'getUser']);
    Route::post('logout', [AuthController::class, 'logout']);

    Route::post('get_products', [ProductController::class, 'getProducts']);
    Route::post('product-details', [ProductController::class, 'getProductDetails']);

    Route::post('cart-add-item', [CartController::class, 'cartAddItem']);
    Route::post('cart-delete-item', [CartController::class, 'cartDeleteItem']);
    Route::post('get-cart-items', [CartController::class, 'getCartItems']);

    Route::post('checkout', [CartController::class, 'checkout']);
});
